//
//  ViewController.swift
//  Quiz
//
//  Created by Jay on 19/05/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    //this is to connect from controler to the objects on the story board
    //connecting to the labels
    
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    //let is constant.
    
    //this will store the questions.
    let questions:[String] =
    [
        "who is Batman?",
        "no you are not",
        "are you sure?"
    ]
    
    //this will store the answers.
    let answers: [String] =
    [
        "I am Batman!",
        "I AM BATMAN!",
        "Check out my Batarang man!"
    ]
    
    //index that will keep track of the button presses
    var currentQuestionIndex: Int = 0
    
    //the question mark string
    let question_mark: String = "?????"
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        questionLabel.text = questions[currentQuestionIndex]
    }
    
    //connecting to button. actions
    @IBAction func showNextQuestion(_ sender: UIButton)
    {
        //evertime this button is pressed, show the next question.
        currentQuestionIndex+=1
        
        //if the current question has reached the limit which is 0,1,2 here, make it 0.
        if(currentQuestionIndex == questions.count)
        {
            currentQuestionIndex = 0;
        }
        
        //get the question
        let question: String = questions[currentQuestionIndex]
        //set the question as the text of the question label
        questionLabel.text = question
        //set the answer to ??? mark
        answerLabel.text = question_mark
    }
    
    @IBAction func showAnswer(_ sender:UIButton)
    {
        //get the answer
        let answer: String = answers[currentQuestionIndex]
        //attach the answer to label
        answerLabel.text = answer
    }
    

}

